// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

module gitlab.com/zygoon/go-wsl

go 1.21.1

require golang.org/x/text v0.13.0
