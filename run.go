// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package wsl

import (
	"context"
	"fmt"
	"os/exec"
)

// CommandOptions influence how a command is executed inside WSL
type CommandOptions struct {
	ShellType ShellType // Type of shell to use (standard, login or none).
	Directory string    // Working directory for the process.
	User      string    // User to run as.
	UseShell  bool      // Use the shell to interpret the command.
}

// CommandContext returns a command ready to execute in the given distribution.
func (d Distribution) CommandContext(ctx context.Context, opts *CommandOptions, name string, args ...string) *exec.Cmd {
	cmd := exec.CommandContext(ctx, wslExe)

	if d.system {
		cmd.Args = append(cmd.Args, "--system")
	} else if d.Name != "" {
		cmd.Args = append(cmd.Args, "--distribution", d.Name)
	}

	if opts != nil {
		if opts.ShellType != "" {
			cmd.Args = append(cmd.Args, "--shell-type", string(opts.ShellType))
		}

		if opts.Directory != "" {
			cmd.Args = append(cmd.Args, "--cd", opts.Directory)
		}

		if opts.User != "" {
			cmd.Args = append(cmd.Args, "--user", opts.User)
		}
	}

	if opts != nil && opts.UseShell {
		cmd.Args = append(cmd.Args, name)
	} else {
		cmd.Args = append(cmd.Args, "--exec", name)
	}

	cmd.Args = append(cmd.Args, args...)

	return cmd
}

// ShellType is the type of shell to use when executing a command.
type ShellType string

// Known values of ShellType.
//
// The semantics inside WSL is hard to understand.
const (
	Standard ShellType = "standard"
	Login    ShellType = "login"
	None     ShellType = "none"
)

// String returns "standard", "login" or "none"
//
// String implements flag.Value.
func (st ShellType) String() string {
	return string(st)
}

// Set sets the shell type to the given string or returns an error if the type is unknown.
//
// Set implements flag.Value.
func (st *ShellType) Set(s string) error {
	switch s {
	case Standard.String():
		*st = Standard
	case Login.String():
		*st = Login
	case None.String():
		*st = None
	default:
		return fmt.Errorf("unsupported value: must be one of %s, %s or %s", Standard, Login, None)
	}

	return nil
}

// Get implements flag.Getter.
func (st ShellType) Get() any {
	return st
}
