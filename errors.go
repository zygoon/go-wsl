// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package wsl

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"os/exec"

	"golang.org/x/text/encoding/unicode"
)

// Known errors returned by WSL.
var (
	ErrDistributionNotFound        = errors.New("distribution not found")
	ErrDefaultDistributionNotFound = errors.New("default distribution not found")
	ErrVersionParse                = errors.New("cannot parse version")
	ErrNameNotResolved             = errors.New("cannot resolve DNS name")
	ErrVersion1NotSupported        = errors.New("WSL1 is not supported")
)

func parseError(errText string) error {
	switch errText {
	case "Wsl/Service/WSL_E_DISTRO_NOT_FOUND": // spell-checker: disable-line
		return ErrDistributionNotFound
	case "Wsl/Service/CreateInstance/GetDefaultDistro/WSL_E_DEFAULT_DISTRO_NOT_FOUND": // spell-checker: disable-line
		return ErrDefaultDistributionNotFound
	case "Wsl/ERROR_VERSION_PARSE_ERROR":
		return ErrVersionParse
	case "Wsl/WININET_E_NAME_NOT_RESOLVED": // spell-checker: disable-line
		return ErrNameNotResolved
	case "Wsl/WSL_E_WSL1_NOT_SUPPORTED":
		return ErrVersion1NotSupported
	default:
		return fmt.Errorf("unknown error: %s", errText)
	}
}

func scanOutput(ctx context.Context, args []string, fn func(sc *bufio.Scanner) error) error {
	cmd := exec.CommandContext(ctx, wslExe)
	cmd.Args = append(cmd.Args, args...)

	// NOTE: wsl.exe doesn't print errors to standard error so we must buffer and
	// re-parse the output if the command fails. In practice output is fairly
	// short but the design is a bit poor.
	stdoutBlob, err := cmd.Output()
	// NOTE: We are checking errors later, hold tight.
	// NOTE: wsl.exe is producing output in Windows "wide" UTF-16 encoding.
	stdout := unicode.UTF16(
		unicode.LittleEndian, unicode.UseBOM,
	).NewDecoder().Reader(bytes.NewReader(stdoutBlob))
	sc := bufio.NewScanner(stdout)

	if err != nil {
		// On error look for the prefix "Error code: " and map the remainder to
		// a well-known error value.
		const prefix = "Error code: "

		for sc.Scan() {
			if line := sc.Bytes(); bytes.HasPrefix(line, []byte(prefix)) {
				return parseError(string(bytes.TrimPrefix(line, []byte(prefix))))
			}
		}

		// In case the protocol is not upheld, we at least return the error from exec.
		return err
	}

	// We are not interested in the output
	if fn == nil {
		return nil
	}

	// Scan the output with the help of the callback.
	if err := fn(sc); err != nil {
		return err
	}

	return sc.Err()
}
