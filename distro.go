// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package wsl

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"os/exec"
)

var errSystemDistribution = errors.New("unsupported for system distribution")

// Distribution is a container running under WSL or WSL2 management.
//
// Distributions are identified by name. A special, unnamed system distribution
// exists to manage WSL itself. That distribution has certain limitations, as
// inability to become the default distribution, be terminated or removed.
type Distribution struct {
	Name         string // Select distribution with the given name.
	FriendlyName string // Only when populated by ListOnline.
	system       bool   // Select the CBL-Mariner/Linux WSL-system distribution.
}

// DefaultDistribution returns the default distribution to use.
func DefaultDistribution(ctx context.Context) (d Distribution, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot find default distribution: %w", err)
		}
	}()

	err = scanOutput(ctx, []string{"--status"}, func(sc *bufio.Scanner) error {
		for lineno := 1; sc.Scan(); lineno++ {
			// Status displays a number of other elements that we are not interested in.
			if lineno != 1 {
				continue
			}

			fields := bytes.FieldsFunc(sc.Bytes(), func(r rune) bool { return r == ':' })
			if len(fields) < 2 {
				continue
			}

			d = Distribution{Name: string(bytes.TrimSpace(fields[1]))}
		}

		return nil
	})

	if d.Name == "" {
		return d, fmt.Errorf("no default distribution found")
	}

	return d, err
}

// SetDefaultDistributions sets the given as the default.
func SetDefaultDistribution(ctx context.Context, d Distribution) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot set default distribution: %w", err)
		}
	}()

	if d.system {
		return errSystemDistribution
	}

	return scanOutput(ctx, []string{"--set-default", d.Name}, nil)
}

// SystemDistribution is an unnamed distribution used to operate WSL itself.
func SystemDistribution() Distribution {
	return Distribution{system: true}
}

// String returns the name of the distribution.
func (d Distribution) String() string {
	if d.system {
		return "(system distribution)"
	}

	return d.Name
}

// Terminate forcibly stops the given distribution.
func (d Distribution) Terminate(ctx context.Context) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot terminate distribution: %w", err)
		}
	}()

	if d.system {
		return errSystemDistribution
	}

	return scanOutput(ctx, []string{"--terminate", d.Name}, nil)
}

// SetVersion sets the version of WSL to use for the given distribution.
func (d Distribution) SetVersion(ctx context.Context, v Version) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot set WSL version: %w", err)
		}
	}()

	if d.system {
		return errSystemDistribution
	}

	return exec.CommandContext(ctx, wslExe, "--set-version", d.Name, string(v)).Run()
}
