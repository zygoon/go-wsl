// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

// Package WSL implements Go bindings for Windows Subsystem for Linux
// entry-point program wsl.exe.
package wsl

import (
	"context"
	"fmt"
)

// wslExe is the entry point for working with WSL.
const wslExe = "wsl.exe"

// Shutdown stops all running distributions and shuts down WSL2 virtual machine.
func Shutdown(ctx context.Context) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot shut down WSL: %w", err)
		}
	}()

	return scanOutput(ctx, []string{"--shutdown"}, nil)
}
