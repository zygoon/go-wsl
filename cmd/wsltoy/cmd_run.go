// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-wsl"
)

func runCmd() cmdr.Cmd {
	return FuncX("Run a program in WSL", func(ctx context.Context, args []string) error {
		d := wsl.Distribution{} // Default distribution
		system := false         //
		opts := wsl.CommandOptions{}

		fl := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
		fl.SetOutput(os.Stderr)
		fl.Usage = func() {
			fmt.Fprintf(fl.Output(), "Usage: %s PROGRAM [ARGUMENTS]...\n", fl.Name())
			fl.PrintDefaults()
		}

		const (
			flagSystem = "system"
			flagDistro = "d"
		)

		fl.StringVar(&d.Name, flagDistro, "", "Distribution to run the command in")
		fl.BoolVar(&system, flagSystem, false, "Use CBL-Mariner/Linux (internal WSL distribution)")
		fl.StringVar(&opts.Directory, "C", "", "Use the given working directory")
		fl.StringVar(&opts.User, "u", "", "Run as the given user")
		fl.BoolVar(&opts.UseShell, "sh", false, "Run as a shell command (unsafe)")
		fl.Var(&opts.ShellType, "t", "Type of shell to use (standard, login, none)")

		if err := fl.Parse(args); err != nil {
			return err
		}

		n := 0
		fl.Visit(func(f *flag.Flag) {
			if f.Name == flagSystem || f.Name == flagDistro {
				n++
			}
		})
		if n == 2 {
			return fmt.Errorf("cannot specify both -system and -d")
		}

		if fl.NArg() == 0 {
			return errors.New("missing command to run")
		}

		if system {
			d = wsl.SystemDistribution()
		}

		cmd := d.CommandContext(ctx, &opts, fl.Arg(0), fl.Args()[1:]...)

		// Connect all I/O streams.
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		return cmd.Run()
	})
}
