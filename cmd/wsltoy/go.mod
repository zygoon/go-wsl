// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

module gitlab.com/zygoon/go-wsl/cmd/wsltoy

go 1.21.1

replace gitlab.com/zygoon/go-wsl => ../..

require (
	gitlab.com/zygoon/go-cmdr v1.9.0
	gitlab.com/zygoon/go-wsl v0.1.0
)

require golang.org/x/text v0.13.0 // indirect
