// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-wsl"
)

type defaultDistroCmd struct{}

func (cmd defaultDistroCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (defaultDistroCmd) OneLiner() string {
	return "Display or set the default distribution"
}

func (defaultDistroCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (defaultDistroCmd) FlagUsage(fl *flag.FlagSet) {
	w := fl.Output()

	_, _ = fmt.Fprintf(w, "Usage: %s [DISTRIBUTION]\n", fl.Name())
	_, _ = fmt.Fprintf(w, "\n"+"If DISTRIBUTION is not specified, the current default is displayed.\n")
	_, _ = fmt.Fprintf(w, "If DISTRIBUTION is specified, it will be set as the default.\n")
}

func (defaultDistroCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	if fl.NArg() == 0 {
		d, err := wsl.DefaultDistribution(ctx)
		if err != nil {
			return err
		}

		_, _ = fmt.Println(d.Name)

		return nil
	}

	return wsl.SetDefaultDistribution(ctx, wsl.Distribution{Name: fl.Arg(0)})
}
