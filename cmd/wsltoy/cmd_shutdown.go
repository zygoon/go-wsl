// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-wsl"
)

type shutdownCmd struct{}

func (cmd shutdownCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (shutdownCmd) OneLiner() string {
	return "Shutdown all of WSL"
}

func (shutdownCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (shutdownCmd) FlagUsage(fl *flag.FlagSet) {
	w := fl.Output()

	_, _ = fmt.Fprintf(w, "Usage: %s\n", fl.Name())
	_, _ = fmt.Fprintf(w, "\n"+"All running WSL distributions are terminated.\n")
	_, _ = fmt.Fprintf(w, "In the case of WSL2, the dedicated virtual machine is shut down.")
}

func (shutdownCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	return wsl.Shutdown(ctx)
}
