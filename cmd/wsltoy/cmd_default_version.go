// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package main

import (
	"context"
	"flag"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-wsl"
)

type defaultVersionCmd struct{}

func (cmd defaultVersionCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (defaultVersionCmd) OneLiner() string {
	return "Display or set the default WSL version"
}

func (defaultVersionCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	return flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)
}

func (defaultVersionCmd) FlagUsage(fl *flag.FlagSet) {
	w := fl.Output()

	_, _ = fmt.Fprintf(w, "Usage: %s [VERSION]\n", fl.Name())
	_, _ = fmt.Fprintf(w, "\n"+"If VERSION is not specified, the current default is displayed.\n")
	_, _ = fmt.Fprintf(w, "If VERSION is specified, it will be set as the default.\n")
	_, _ = fmt.Fprintf(w, "\n"+"VERSION is either 1 or 2.\n")
}

func (defaultVersionCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	if fl.NArg() == 0 {
		v, err := wsl.DefaultVersion(ctx)
		if err != nil {
			return err
		}

		_, _ = fmt.Println(v)

		return nil
	}

	return wsl.SetDefaultVersion(ctx, wsl.Version(fl.Arg(0)))
}
