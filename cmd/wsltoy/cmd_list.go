// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package main

import (
	"context"
	"fmt"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
	"gitlab.com/zygoon/go-wsl"
)

func listCmd() cmdr.Cmd {
	return &router.Cmd{
		OneLinerText: "List installed (default), running and online distributions",
		Commands: map[string]cmdr.Cmd{
			"installed": listInstalledCmd(),
			"running":   listRunningCmd(),
			"online":    listOnlineCmd(),
		},
	}
}

func listInstalledCmd() cmdr.Cmd {
	return FuncX("List distributions installed locally", func(ctx context.Context, args []string) error {
		ds, err := wsl.ListInstalled(ctx)
		if err != nil {
			return err
		}

		for _, d := range ds {
			_, _ = fmt.Println(d.Name)
		}

		return err
	})
}

func listRunningCmd() cmdr.Cmd {
	return FuncX("List distributions with running processes", func(ctx context.Context, args []string) error {
		ds, err := wsl.ListRunning(ctx)
		if err != nil {
			return err
		}

		for _, d := range ds {
			_, _ = fmt.Println(d.Name)
		}

		return err
	})
}

func listOnlineCmd() cmdr.Cmd {
	return FuncX("List distributions available online", func(ctx context.Context, args []string) error {
		ds, err := wsl.ListOnline(ctx)
		if err != nil {
			return err
		}

		for _, d := range ds {
			_, _ = fmt.Printf("%s (%s)\n", d.Name, d.FriendlyName)
		}

		return err
	})
}
