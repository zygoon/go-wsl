// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package main

import (
	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
)

func defaultCmd() cmdr.Cmd {
	return &router.Cmd{
		OneLinerText: "Display and set defaults",
		Commands: map[string]cmdr.Cmd{
			"distro":  defaultDistroCmd{},
			"version": defaultVersionCmd{},
		},
	}
}
