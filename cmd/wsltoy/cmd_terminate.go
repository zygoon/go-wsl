// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package main

import (
	"context"
	"flag"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/flagcmd"
	"gitlab.com/zygoon/go-wsl"
)

type terminateCmd struct {
	distro wsl.Distribution
}

func (cmd *terminateCmd) Run(ctx context.Context, args []string) error {
	return flagcmd.MakeCmd(cmd).Run(ctx, args)
}

func (*terminateCmd) OneLiner() string {
	return "Terminate a WSL distribution"
}

func (cmd *terminateCmd) FlagSet(ctx context.Context) *flag.FlagSet {
	fl := flag.NewFlagSet(cmdr.Name(ctx), flag.ContinueOnError)

	fl.StringVar(&cmd.distro.Name, "d", "", "Distribution to terminate")

	return fl
}

func (cmd *terminateCmd) RunFlag(ctx context.Context, fl *flag.FlagSet) error {
	return cmd.distro.Terminate(ctx)
}
