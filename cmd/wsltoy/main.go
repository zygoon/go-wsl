// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package main

import (
	"context"
	"os"

	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
)

func main() {
	cmdr.RunMain(mainCmd(), os.Args)
}

func mainCmd() cmdr.Cmd {
	return &router.Cmd{
		OneLinerText: "WSL Toy - a simple WSL management tool",
		Commands: map[string]cmdr.Cmd{
			"default":   defaultCmd(),
			"shutdown":  shutdownCmd{},
			"list":      listCmd(),
			"run":       runCmd(),
			"terminate": &terminateCmd{},
		},
	}
}

func FuncX(oneLiner string, run func(context.Context, []string) error) cmdX {
	return cmdX{run: run, oneLiner: oneLiner}
}

type cmdX struct {
	oneLiner string
	run      func(context.Context, []string) error
}

func (cmd cmdX) OneLiner() string {
	return cmd.oneLiner
}

func (cmd cmdX) Run(ctx context.Context, args []string) error {
	return cmd.run(ctx, args)
}
