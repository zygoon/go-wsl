// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package wsl

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
)

// Version is the major version of WSL itself.
//
// WSL has two versions as of this time: WSL1 with Windows emulating Linux and
// WSL2 with Windows running Linux inside a virtual machine.
type Version string

// Available versions of WSL.
const (
	// Version1 or WSL1 is the emulated Linux environment running directly on top of Windows kernel.
	//
	// Version1 has numerous limitations and does not support large chunks of Linux APIs.
	// Version1 has better interoperability with Windows file systems (access is faster).
	Version1 Version = "1"
	// Version2 or WSL2 is the virtualized Linux environment running inside Hyper-V.
	//
	// Version2 has access to large number of Linux-specific APIs.
	// Version2 has much better performance when accessing Linux file systems and
	// degraded performance when accessing Windows file systems, at least as
	// compared to Wsl1.
	Version2 Version = "2"
)

// DefaultVersion returns the version of WSL to use by default for new distributions.
func DefaultVersion(ctx context.Context) (ver Version, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot find default version: %w", err)
		}
	}()

	err = scanOutput(ctx, []string{"--status"}, func(sc *bufio.Scanner) error {
		for lineno := 1; sc.Scan(); lineno++ {
			if lineno != 2 {
				continue
			}

			fields := bytes.FieldsFunc(sc.Bytes(), func(r rune) bool { return r == ':' })
			if len(fields) < 2 {
				continue
			}

			switch verStr := string(bytes.TrimSpace(fields[1])); verStr {
			case "1":
				ver = Version1
			case "2":
				ver = Version2
			default:
				return fmt.Errorf("unknown version: %s", verStr)
			}
		}

		return nil
	})

	if ver == "" {
		return "", fmt.Errorf("no default version found")
	}

	return ver, err
}

// SetDefaultVersion sets the version of WSL to use by default for new distributions.
func SetDefaultVersion(ctx context.Context, v Version) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("cannot set default version: %w", err)
		}
	}()

	return scanOutput(ctx, []string{"--set-default-version", string(v)}, nil)
}
