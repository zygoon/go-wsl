// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Zygmunt Krynicki

//go:build: +windows

package wsl

import (
	"bufio"
	"bytes"
	"context"
)

// ListInstalled returns the list of installed WSL distributions.
func ListInstalled(ctx context.Context) ([]Distribution, error) {
	var ds []Distribution

	err := scanOutput(ctx, []string{"--list", "--quiet"}, func(sc *bufio.Scanner) error {
		for sc.Scan() {
			ds = append(ds, Distribution{Name: sc.Text()})
		}

		return nil
	})

	return ds, err
}

// ListRunning returns the list of running WSL distributions.
func ListRunning(ctx context.Context) ([]Distribution, error) {
	var ds []Distribution

	err := scanOutput(ctx, []string{"--list", "--quiet", "--running"}, func(sc *bufio.Scanner) error {
		for sc.Scan() {
			ds = append(ds, Distribution{Name: sc.Text()})
		}

		return nil
	})

	return ds, err
}

// ListOnline returns the list of WSL distributions available for installation.
func ListOnline(ctx context.Context) ([]Distribution, error) {
	var ds []Distribution

	err := scanOutput(ctx, []string{"--list", "--quiet", "--online"}, func(sc *bufio.Scanner) error {
		// Skip usage instructions until we reach the header line that starts with "NAME"
		for sc.Scan() {
			if bytes.HasPrefix(sc.Bytes(), []byte("NAME")) {
				break
			}
		}

		// Scan lines after the header as a pairs of: <NAME> <FRIENDLY NAME>
		for sc.Scan() {
			line := sc.Bytes()
			if spIdx := bytes.IndexRune(line, ' '); spIdx != -1 {
				name := line[:spIdx]
				friendlyName := bytes.TrimSpace(line[spIdx:])
				ds = append(ds, Distribution{
					Name:         string(name),
					FriendlyName: string(friendlyName),
				})
			}
		}

		return nil
	})

	return ds, err
}
